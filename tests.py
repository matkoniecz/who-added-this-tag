import unittest
import who_added_this_tag

class Tests(unittest.TestCase):
    def test_basic_math(self):
        self.assertEqual(2-2, 0)

    def test_example_code(self):
        self.assertEqual(2-2, 0)
        config = {
            'key': 'related:wikipedia',
            'value': None, # may be None - in such case it looks for all values of key
            'area_identifier_key': None, # may be None, in such case search is worldwide, 'ISO3166-1' is a good identifier
            'area_identifier_value': 'PL', # ignored if area_identifier_key is None
            'list_all_edits': True,
            'list_all_edits_made_by_this_users': [], # ignored with list_all_edits set to True
        }
        who_added_this_tag.statistics.process_case(config)

if __name__ == '__main__':
    unittest.main()
